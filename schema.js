const fetch = require('node-fetch');
const itemParser = require('./parsers/item-parser');
const SHOP_PARAMETERS = require('./parameters');
const googleTranslate = require('google-translate-api');


const {
	GraphQLSchema,
	GraphQLObjectType,
	GraphQLInt,
	GraphQLList,
	GraphQLString
} = require('graphql');


const fetchDatas = (root, args) => {

	return Promise.all(
		args.shops.map(elem => {

			if (elem === 'zara') {
				return googleTranslate(args.name, {to: 'en'})
					.then(res => res.text)
					.then((name_en) => {
						let newArgs = args;
						newArgs.name = name_en;
						return fetch(SHOP_PARAMETERS(newArgs)[elem].url)
							.then(resp => resp.text())
							.then(text => itemParser(text, SHOP_PARAMETERS(args)[elem]))
					})
			} else {
				return fetch(SHOP_PARAMETERS(args)[elem].url)
					.then(resp => resp.text())
					.then(text => itemParser(text, SHOP_PARAMETERS(args)[elem]))
			}
			}
		)
	)
};

const resultItemType = new GraphQLObjectType({
	name: 'Result',
	description: '...',

	fields: () => ({
		name: {
			type: GraphQLString,
			resolve: item => item.name
		},
		image: {
			type: GraphQLString,
			resolve: item => item.image
		},
		price: {
			type: GraphQLString,
			resolve: item => item.price
		},
		originPrice: {
			type: GraphQLString,
			resolve: item => item.originPrice
		},
		type: {
			type: GraphQLString,
			resolve: item => item.type
		},
		url: {
			type: GraphQLString,
			resolve: item => item.url
		},
	})
});

module.exports = new GraphQLSchema({
	query: new GraphQLObjectType({
		name: 'Query',
		description: '...',

		fields: () => ({
			results: {
				type: new GraphQLList(resultItemType),
				args: {
					name: {type: GraphQLString},
					size: {type: GraphQLInt},
					shops: {type: new GraphQLList(GraphQLString)},
					gender: {type: GraphQLString}
				},

				resolve: (root, args) => fetchDatas(root, args)
					.then(html => {
						let newArray = [];
						html.map((shops, i) => shops.map(item => newArray.push(item)));
						return newArray;
					})
			}
		})
	})
});