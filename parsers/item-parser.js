const hmParser = require('./hm.parser');
const bershkaParser = require('./bershka.parser');
const zaraParser = require('./zara.parser');

const generateResults = (html, shopParams) => {

		if (shopParams.type === 'hm') {
			return hmParser(html, shopParams);
		}

    if (shopParams.type === 'bershka') {
			return bershkaParser(html, shopParams);
    }

    if (shopParams.type === 'zara') {
			return zaraParser(html, shopParams);
    }
};

module.exports = (html, shopParams) => generateResults(html, shopParams);