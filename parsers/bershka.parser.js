const parseBershka = (html, shopParams) => {
	let object = JSON.parse(html);
	let resultArray = [];

	object.content.docs.map((el, i) => {
		let resultObj = {};

		resultObj.name = el.name;
		resultObj.image = `https://static.bershka.net/4/photos2${el.img.url}_1_1_4.jpg`;
		resultObj.price = el.maxPrice.replace('.0', '') + ' Ft';
		resultObj.originPrice = (el.minPrice !== el.maxPrice) ? el.minPrice : null;
		resultObj.type = shopParams.type;
		resultObj.url = `https://www.bershka.com/hu/${el.name}-c0p${el.id}.html?colorId=${el.colorId}`;

		if (resultObj.originPrice !== null) {
			console.log(resultObj.originPrice)
		}

		resultArray.push(resultObj)

	});

	return resultArray
};

module.exports = (html, shopParams) => parseBershka(html, shopParams);