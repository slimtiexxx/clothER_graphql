const translate = require('google-translate-api');

const parseZara = (html, shopParams) => {
	let obj = JSON.parse(html);

	resultArray = [];
	obj.products.map((el, i) => {
		let resultObj = {};

		resultObj.name = el.detail.name;
		resultObj.image = `https://static.zara.net/photos//${el.xmedia[1].path}/w/400/${el.xmedia[1].name}.jpg?ts=${el.xmedia[1].timestamp}`;
		resultObj.price = ((el.price-(el.price%100))/100).toString() + ' Ft';
		resultObj.type = shopParams.type;
		resultObj.url = `https://www.zara.com/hu/en/${el.seo.keyword}-p${el.seo.seoProductId}.html`;

		resultArray.push(resultObj)

	});

	return resultArray
};

module.exports = (html, shopParams) => parseZara(html, shopParams);