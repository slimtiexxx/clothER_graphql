const cheerio = require('cheerio');
const $ = (el) => cheerio.load(el);

const getImageUrl = (html) => {
	let $ = cheerio.load(html);
	let results = [];

	$('.product-item-image').each((i, element) => {
		results.push($(element).attr('src'));
	});

	return results;
};

const getName = (html) => {
	let $ = cheerio.load(html);
	let results = [];

	$('.product-item-heading > a').each((i, element) => {
		results.push($(element).text());
	});

	return results;
};

const getLink = (html) => $(html)('.product-item-link').map((i, el) => 'www2.hm.com' + $(html)(el).attr('href').replace('.genericdevice', '').replace('m/', ''));

const getPrice = (html) => $(html)('.product-item-details').children('div:not(div.ng-hide)').map( (i, el) => $(html)(el).children().contents().first().text() );
const getOriginPrice = (html) => $(html)('.product-item-details').children('div:not(div.ng-hide)').map( (i, el) => ($(html)(el).children().children().length) ? $(html)(el).children().children().contents().first().text() : '');

const parseHm = (html, shopParams) => {
	const names = getName(html);
	const images = getImageUrl(html);
	const prices = getPrice(html);
	const originPrices = getOriginPrice(html);
	const links = getLink(html);

	let resultArray = [];

	names.forEach(function (item, i) {
		let resultObj = {};

		resultObj.name = item;
		resultObj.image = images[i];
		resultObj.price = prices[i];
		resultObj.originPrice = originPrices[i] ? originPrices[i] : null;
		resultObj.url = links[i];
		resultObj.type = shopParams.type;

		resultArray.push(resultObj)

	});

	return resultArray
};

module.exports = (html, shopParams) => parseHm(html, shopParams);