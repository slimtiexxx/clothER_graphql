const googleTranslate = require('google-translate-api');
const slug = require('slug');

const SHOP_PARAMETERS = (args) => {

	return {
		"hm": {
			"url": `http://www2.hm.com/m/hu_hu/search-results/_jcr_content/main/mobilesearch.display.genericdevice.html?q=${args.name}&offset=0&page-size=${args.size}`,
			"type": "hm"
		},
		"bershka": {
			"url": `https://sbbershka.empathybroker.com/sb-bershka/services/search?&rows=${args.size}&start=0&q=${slug(args.name)}&lang=hu&scope=desktop&store=45109550&topTrends.rows=10&catalogue=40259532&warehouse=42109533&weighted=1010193133`,
			"type": "bershka"
		},
		"zara": {
			"url": `https://api.empathybroker.com/search/v1/query/zara/search?&o=json&q=${args.name}&scope=default&t=*&lang=en_US&store=11743&catalogue=27052&warehouse=23553&start=0&rows=${args.size}`,
			"type": "zara"
		}
	};
};

module.exports = (args) => SHOP_PARAMETERS(args);