const express = require('express');
const graphqlHTTP = require('express-graphql');
const cors = require('cors');
const app = express();

const schema = require('./schema');

app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true
}));

app.use(cors());

app.get('/graphql', function (req, res, next) {
    res.json({msg: 'This is CORS-enabled for all origins!'})
});

app.listen(process.env.PORT || 4000);
console.log('Listening on port 4000');